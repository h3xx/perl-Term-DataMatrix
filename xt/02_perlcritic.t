#!perl
use 5.012;
use warnings FATAL => 'all';

use Test::More;

unless (eval {
    require Test::Perl::Critic;
}) {
    plan(skip_all => 'Test::Perl::Critic is not installed.');
}
Test::Perl::Critic->import( -profile => 'xt/perlcriticrc');
all_critic_ok('lib');
