#!perl
use 5.012;
use warnings FATAL => 'all';

use Test::More;

unless (eval {
    require Test::Spelling;
}) {
    plan(skip_all => 'Test::Spelling is not installed.');
}
Test::Spelling->import;
add_stopwords(map { split /[\s\:\-]/ } <DATA>);
local $ENV{LANG} = 'C';
all_pod_files_spelling_ok('lib');
__DATA__
CPAN
Term::DataMatrix
barcode
barcodes
dchurch
gmx
