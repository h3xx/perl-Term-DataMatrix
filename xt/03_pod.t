#!perl
use 5.012;
use warnings FATAL => 'all';

use Test::More;

unless (eval {
    require Test::Pod;
}) {
    plan(skip_all => 'Test::Pod 1.00 required for testing POD');
}
Test::Pod->import;
all_pod_files_ok();
