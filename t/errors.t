#!perl
use 5.012;
use warnings FATAL => 'all';

use Test::More 'no_plan';

require_ok('Term::DataMatrix');

my $dmcode = Term::DataMatrix->new;

if (eval {
    $dmcode->plot;
}) {
    fail('->plot() with no arguments should abort');
}

# Error message should mention why.
like($@, qr/arguments/,
    '->plot() with no arguments should mention why it aborted'
);
